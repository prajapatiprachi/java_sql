FROM gradle:7.6-jdk11

ADD build/libs/crud-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8081

CMD ["java", "-jar", "/app.jar"]
